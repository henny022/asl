# Kamiko

## Features:
- auto start (set "Start Timer at:" to 0.45)
- in game time
- split on map change
- split on final golden torii
- split on health and sp upgrade

## Quirks
- to make IGT work, you need to toggle the timer display in the game once

## TODO
- make IGT work without extra actions
