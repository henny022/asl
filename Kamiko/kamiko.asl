state("Kamiko")
{
    string26 map_name : "GameAssembly.dll", 0xcb3b08, 0xb8, 0, 0x10, 0x14;
    int total_time : "GameAssembly.dll", 0xcb8310, 0x18, 0xb8, 0, 0x40, 0x40;
    float map_time : "GameAssembly.dll", 0xcb8310, 0x18, 0xb8, 0, 0x40, 0x44;
    int hp : "GameAssembly.dll", 0xcb3af8, 0xb8, 0x8, 0x10, 0x18;
    int max_hp : "GameAssembly.dll", 0xcb3af8, 0xb8, 0x8, 0x10, 0x1c;
    int sp : "GameAssembly.dll", 0xcb3af8, 0xb8, 0x8, 0x10, 0x30;
    int max_sp : "GameAssembly.dll", 0xcb3af8, 0xb8, 0x8, 0x10, 0x34;
    string20 next_scene : "GameAssembly.dll", 0xcb83b8, 0x18, 0xb8, 0, 0x30, 0x14;
}

start
{
    if (current.next_scene != old.next_scene && current.next_scene == "Prologue")
        return true;
    return false;
}

split
{
    if ((current.map_name != old.map_name) && settings[current.map_name])
        return true;
    if (current.sp == 0 && old.sp != 0 && current.map_name == "kamiko_map_51" && settings["sp_finish"])
        return true;
    if (current.max_hp > old.max_hp && old.max_hp != 0 && settings["hp_up"])
        return true;
    if (current.max_sp > old.max_sp && old.max_sp != 0 && settings["sp_up"])
        return true;
    if (current.next_scene != old.next_scene && current.next_scene == "Prologue" && settings["prologue"])
        return true;
    return false;
}

reset
{
    if (current.next_scene != old.next_scene && current.next_scene == "Title")
        return true;
    return false;
}

gameTime
{
    if (current.next_scene == "Prologue")
        return TimeSpan.FromSeconds(0);
    return TimeSpan.FromSeconds(current.total_time + current.map_time);
}

isLoading
{
    if (current.next_scene == "Prologue")
        return true;
    return current.map_time == old.map_time;
}

startup
{
    settings.Add("map_splits", true, "Map Splits");
    settings.Add("kamiko_map_11", false, "Enter Forest of Awakening", "map_splits");
    settings.SetToolTip("kamiko_map_11", "this is at the very start of the run, you probably don't want this for single character runs\nmight produce bugs");
    settings.Add("kamiko_map_12", false, "Enter Forest Boss", "map_splits");
    settings.Add("kamiko_map_21", true, "Enter Sunken Relics", "map_splits");
    settings.Add("kamiko_map_22", false, "Enter Water Boss", "map_splits");
    settings.Add("kamiko_map_31", true, "Enter Scorching Labyrinth", "map_splits");
    settings.Add("kamiko_map_32", false, "Enter Volcano Boss", "map_splits");
    settings.Add("kamiko_map_41", true, "Enter Ruins of Yamataikoku", "map_splits");
    settings.Add("kamiko_map_42", false, "Enter Last Boss", "map_splits");
    settings.Add("kamiko_map_51", false, "Enter Epilogue", "map_splits");
    settings.Add("other_splits", true, "Other Splits");
    settings.Add("sp_finish", true, "Golden Torii Finish", "other_splits");
    settings.SetToolTip("sp_finish", "I'm stupid");
    settings.Add("hp_up", false, "Health Upgrade", "other_splits");
    settings.Add("sp_up", false, "SP Upgrade", "other_splits");
    settings.Add("prologue", false, "Prologue", "other_splits");
    settings.SetToolTip("prologue", "Used to start the timer, use this for multi character runs");
}
