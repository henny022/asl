# LiveSplit Auto Splitter Scripts

## Usage
To use one of the scripts, download and save it somewhere.
Then add a `Scriptable Auto Splitter` component to you LiveSplit layout.
The script should now be working.
You can configure script settings in the layout.
